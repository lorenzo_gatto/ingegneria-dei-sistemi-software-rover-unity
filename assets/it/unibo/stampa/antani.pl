%====================================================================================
% Context stampa  SYSTEM-configuration: file it.unibo.stampa.antani.pl 
%====================================================================================
context(stampa, "localhost",  "TCP", "8000" ).  		 
%%% -------------------------------------------
qactor( dashboardx , stampa, "it.unibo.dashboardx.MsgHandle_Dashboardx"   ). %%store msgs 
qactor( dashboardx_ctrl , stampa, "it.unibo.dashboardx.Dashboardx"   ). %%control-driven 
qactor( sonarx , stampa, "it.unibo.sonarx.MsgHandle_Sonarx"   ). %%store msgs 
qactor( sonarx_ctrl , stampa, "it.unibo.sonarx.Sonarx"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evh,stampa,"it.unibo.stampa.Evh","sonarnear,obstacle,start,stop,takepicture").  
eventhandler(savecmd,stampa,"it.unibo.stampa.Savecmd","usercmdStart").  
%%% -------------------------------------------

