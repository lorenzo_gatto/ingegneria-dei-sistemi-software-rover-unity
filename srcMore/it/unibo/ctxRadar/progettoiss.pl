%====================================================================================
% Context ctxRadar  SYSTEM-configuration: file it.unibo.ctxRadar.progettoISS.pl 
%====================================================================================
context(ctxdashboard, "localhost",  "TCP", "8088" ).  		 
context(ctxradar, "localhost",  "TCP", "8111" ).  		 
%%% -------------------------------------------
qactor( radarformatconverter , ctxradar, "it.unibo.radarformatconverter.MsgHandle_Radarformatconverter"   ). %%store msgs 
qactor( radarformatconverter_ctrl , ctxradar, "it.unibo.radarformatconverter.Radarformatconverter"   ). %%control-driven 
qactor( radargui , ctxradar, "it.unibo.radargui.MsgHandle_Radargui"   ). %%store msgs 
qactor( radargui_ctrl , ctxradar, "it.unibo.radargui.Radargui"   ). %%control-driven 
qactor( testermqtt , ctxradar, "it.unibo.testermqtt.MsgHandle_Testermqtt"   ). %%store msgs 
qactor( testermqtt_ctrl , ctxradar, "it.unibo.testermqtt.Testermqtt"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evhconverter,ctxradar,"it.unibo.ctxRadar.Evhconverter","sonar").  
%%% -------------------------------------------

