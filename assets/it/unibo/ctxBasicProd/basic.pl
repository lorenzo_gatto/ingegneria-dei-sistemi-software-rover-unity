%====================================================================================
% Context ctxBasicProd  SYSTEM-configuration: file it.unibo.ctxBasicProd.basic.pl 
%====================================================================================
context(ctxbasicprod, "localhost",  "TCP", "8088" ).  		 
context(ctxbasiccons, "localhost",  "TCP", "8090" ).  		 
%%% -------------------------------------------
qactor( producer , ctxbasicprod, "it.unibo.producer.MsgHandle_Producer"   ). %%store msgs 
qactor( producer_ctrl , ctxbasicprod, "it.unibo.producer.Producer"   ). %%control-driven 
qactor( consumer , ctxbasiccons, "it.unibo.consumer.MsgHandle_Consumer"   ). %%store msgs 
qactor( consumer_ctrl , ctxbasiccons, "it.unibo.consumer.Consumer"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

