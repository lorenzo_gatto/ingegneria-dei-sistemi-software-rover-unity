/* Generated by AN DISI Unibo */ 
package it.unibo.ctxRobot;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class Evhrobot extends AbstractEvhrobot { 
	public Evhrobot(String name, QActorContext myCtx, IOutputEnvView outEnvView, String[] eventIds ) throws Exception {
		super(name, myCtx, outEnvView,eventIds);
  	}
}
