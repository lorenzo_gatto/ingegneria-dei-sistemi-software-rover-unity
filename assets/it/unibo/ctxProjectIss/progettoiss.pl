%====================================================================================
% Context ctxProjectIss  SYSTEM-configuration: file it.unibo.ctxProjectIss.progettoISS.pl 
%====================================================================================
context(ctxprojectiss, "localhost",  "TCP", "8070" ).  		 
%%% -------------------------------------------
qactor( dashboard , ctxprojectiss, "it.unibo.dashboard.MsgHandle_Dashboard"   ). %%store msgs 
qactor( dashboard_ctrl , ctxprojectiss, "it.unibo.dashboard.Dashboard"   ). %%control-driven 
qactor( robot , ctxprojectiss, "it.unibo.robot.MsgHandle_Robot"   ). %%store msgs 
qactor( robot_ctrl , ctxprojectiss, "it.unibo.robot.Robot"   ). %%control-driven 
qactor( test_dashboard_robot_interaction , ctxprojectiss, "it.unibo.test_dashboard_robot_interaction.MsgHandle_Test_dashboard_robot_interaction"   ). %%store msgs 
qactor( test_dashboard_robot_interaction_ctrl , ctxprojectiss, "it.unibo.test_dashboard_robot_interaction.Test_dashboard_robot_interaction"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(evhrobot,ctxprojectiss,"it.unibo.ctxProjectIss.Evhrobot","sonarnear,obstacle,start,stop,alarm").  
eventhandler(evhdashboard,ctxprojectiss,"it.unibo.ctxProjectIss.Evhdashboard","usercmd,sonar").  
%%% -------------------------------------------

