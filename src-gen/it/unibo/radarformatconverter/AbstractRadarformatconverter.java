/* Generated by AN DISI Unibo */ 
package it.unibo.radarformatconverter;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.ActionReceiveTimed;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.IMsgQueue;
import it.unibo.qactors.akka.QActor;


//REGENERATE AKKA: QActor instead QActorPlanned
public abstract class AbstractRadarformatconverter extends QActor { 
	protected AsynchActionResult aar = null;
	protected boolean actionResult = true;
	protected alice.tuprolog.SolveInfo sol;
	//protected IMsgQueue mysupport ;  //defined in QActor
	protected String planFilePath    = null;
	protected String terminationEvId = "default";
	protected String parg="";
	protected boolean bres=false;
	protected IActorAction  action;
	
			protected static IOutputEnvView setTheEnv(IOutputEnvView outEnvView ){
				return outEnvView;
			}
	
	
		public AbstractRadarformatconverter(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
			super(actorId, myCtx,  
			"./srcMore/it/unibo/radarformatconverter/WorldTheory.pl",
			setTheEnv( outEnvView )  , "init");		
			this.planFilePath = "./srcMore/it/unibo/radarformatconverter/plans.txt";
			//Plan interpretation is done in Prolog
			//if(planFilePath != null) planUtils.buildPlanTable(planFilePath);
	 	}
		@Override
		protected void doJob() throws Exception {
			String name  = getName().replace("_ctrl", "");
			mysupport = (IMsgQueue) QActorUtils.getQActor( name ); 
	 		initSensorSystem();
			boolean res = init();
			//println(getName() + " doJob " + res );
			QActorContext.terminateQActorSystem(this);
		} 
		/* 
		* ------------------------------------------------------------
		* PLANS
		* ------------------------------------------------------------
		*/
	    public boolean init() throws Exception{	//public to allow reflection
	    try{
	    	int nPlanIter = 0;
	    	//curPlanInExec =  "init";
	    	boolean returnValue = suspendWork;		//MARCHH2017
	    while(true){
	    	curPlanInExec =  "init";	//within while since it can be lost by switchlan
	    	nPlanIter++;
	    		if( ! planUtils.switchToPlan("printSonar1").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=init WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    public boolean printSonar1() throws Exception{	//public to allow reflection
	    try{
	    	int nPlanIter = 0;
	    	//curPlanInExec =  "printSonar1";
	    	boolean returnValue = suspendWork;		//MARCHH2017
	    while(true){
	    	curPlanInExec =  "printSonar1";	//within while since it can be lost by switchlan
	    	nPlanIter++;
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " ??msg(sonar,\"event\",SENDER,none,sonar(sonar1,TARGET,DISTANCE),SEQNUM)" )) != null ){
	    		temporaryStr = QActorUtils.unifyMsgContent(pengine,"p(Distance,Angle)","p(DISTANCE,0)", guardVars ).toString();
	    		sendMsg("polar","radargui", QActorContext.dispatch, temporaryStr ); 
	    		}
	    		//delay
	    		aar = delayReactive(200,"" , "");
	    		if( aar.getInterrupted() ) curPlanInExec   = "printSonar1";
	    		if( ! aar.getGoon() ) break;
	    		if( ! planUtils.switchToPlan("printSonar2").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=printSonar1 WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    public boolean printSonar2() throws Exception{	//public to allow reflection
	    try{
	    	int nPlanIter = 0;
	    	//curPlanInExec =  "printSonar2";
	    	boolean returnValue = suspendWork;		//MARCHH2017
	    while(true){
	    	curPlanInExec =  "printSonar2";	//within while since it can be lost by switchlan
	    	nPlanIter++;
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " ??msg(sonar,\"event\",SENDER,none,sonar(sonar2,TARGET,DISTANCE),SEQNUM)" )) != null ){
	    		temporaryStr = QActorUtils.unifyMsgContent(pengine,"p(Distance,Angle)","p(DISTANCE,120)", guardVars ).toString();
	    		sendMsg("polar","radargui", QActorContext.dispatch, temporaryStr ); 
	    		}
	    		//delay
	    		aar = delayReactive(200,"" , "");
	    		if( aar.getInterrupted() ) curPlanInExec   = "printSonar2";
	    		if( ! aar.getGoon() ) break;
	    		if( ! planUtils.switchToPlan("printSonar3").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=printSonar2 WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    public boolean printSonar3() throws Exception{	//public to allow reflection
	    try{
	    	int nPlanIter = 0;
	    	//curPlanInExec =  "printSonar3";
	    	boolean returnValue = suspendWork;		//MARCHH2017
	    while(true){
	    	curPlanInExec =  "printSonar3";	//within while since it can be lost by switchlan
	    	nPlanIter++;
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " ??msg(sonar,\"event\",SENDER,none,sonar(sonar3,TARGET,DISTANCE),SEQNUM)" )) != null ){
	    		temporaryStr = QActorUtils.unifyMsgContent(pengine,"p(Distance,Angle)","p(DISTANCE,240)", guardVars ).toString();
	    		sendMsg("polar","radargui", QActorContext.dispatch, temporaryStr ); 
	    		}
	    		//delay
	    		aar = delayReactive(200,"" , "");
	    		if( aar.getInterrupted() ) curPlanInExec   = "printSonar3";
	    		if( ! aar.getGoon() ) break;
	    		if( ! planUtils.switchToPlan("printSonar1").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=printSonar3 WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    protected void initSensorSystem(){
	    	//doing nothing in a QActor
	    }
	    
	 
		/* 
		* ------------------------------------------------------------
		* APPLICATION ACTIONS
		* ------------------------------------------------------------
		*/
		/* 
		* ------------------------------------------------------------
		* QUEUE  
		* ------------------------------------------------------------
		*/
		    protected void getMsgFromInputQueue(){
	//	    	println( " %%%% getMsgFromInputQueue" ); 
		    	QActorMessage msg = mysupport.getMsgFromQueue(); //blocking
	//	    	println( " %%%% getMsgFromInputQueue continues with " + msg );
		    	this.currentMessage = msg;
		    }
	  }
	
