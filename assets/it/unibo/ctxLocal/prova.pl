%====================================================================================
% Context ctxLocal  SYSTEM-configuration: file it.unibo.ctxLocal.prova.pl 
%====================================================================================
context(ctxlocal, "localhost",  "TCP", "8080" ).  		 
%%% -------------------------------------------
qactor( qarep , ctxlocal, "it.unibo.qarep.MsgHandle_Qarep"   ). %%store msgs 
qactor( qarep_ctrl , ctxlocal, "it.unibo.qarep.Qarep"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

